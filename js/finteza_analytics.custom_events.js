(function ($) {
  'use strict';

  jQuery(document).ready(() => {
    $('[data-finteza-event]').bind('click', function () {
      const name = $(this).data('finteza-event');

      if (!!name && typeof fz === 'function') {
        fz('track', name);
      }

      return true;
    });
  });
}(jQuery));
